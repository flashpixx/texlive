# === building base container with dependencies ======================
FROM ubuntu:noble AS texbase

ENV LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    DEBIAN_FRONTEND=noninteractive

RUN apt-get update &&\
    apt-get -y --no-install-recommends install curl wget gnupg ca-certificates locales git git-lfs ghostscript perl lua5.4 openssh-client xz-utils make &&\
    apt-get clean autoclean &&\
    apt-get -y autoremove --purge &&\
    rm -rf /var/lib/apt/lists/*



# === install texlive setup ========================================
FROM texbase AS texinstall
WORKDIR /texinstall

ENV PATH=$PATH:/usr/local/texlive/bin/x86_64-linux

RUN curl -L http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz | tar xz --strip 1
COPY ./texinstall.profile .
RUN ./install-tl -profile ./texinstall.profile;

RUN tlmgr update --self --all --reinstall-forcibly-removed



# === building working container =====================================
FROM texbase AS tex
WORKDIR /latex

ENV TEXMFHOME=/root/texmf \
    PATH=$PATH:/usr/local/texlive/bin/x86_64-linux

COPY ./rootfs /
COPY --from=texinstall /usr/local/texlive /usr/local/texlive

CMD ["latexmk"]
