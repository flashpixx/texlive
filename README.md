# TeXLive

Regularly updated linux-based [TeXLive](https://www.tug.org/texlive/) container image with additional tools like Git, LaTeXMK, Ghostscript, etc. for building TeX documents.

### Worflow

1. Create a directory with the main TeX-file and store all dependencies (files for chapters, images, etc) in subdirectories (the main tex file is on the root level):

    ```
    document.tex
    refernces.bib
    tex/chapter1.tex
    tex/chapter2.tex
    images/myfig.jpg
    ```

2. run the image

   ```
   docker run --rm -it -v <path to your document director>:/latex flashpixx/texlive
   ```

   it generates in the document directory the PDF file
