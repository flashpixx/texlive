default: build

# gets the current version
.PHONY: version
version:
	@docker run --rm -v $(PWD):/src gittools/gitversion:latest-5.0 /src

# container build
.PHONY: build
build:
	@docker compose config
	@docker compose build

# deploy container
.PHONY: deploy
deploy:
	@docker compose push
